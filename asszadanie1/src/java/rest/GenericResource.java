/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rest;

import entity.Students;
import entity.Users;
import facades.StudentsFacade;
import facades.UsersFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;

/**
 * REST Web Service
 *
 * @author husticik
 */
@Path("generic")
public class GenericResource {

    @Context
    private UriInfo context;

    @EJB
    private StudentsFacade students;
    
    @EJB
    private UsersFacade users;
    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
    }

    /**
     * Retrieves representation of an instance of rest.GenericResource
     * @return an instance of java.util.List
     */
    @GET
    @Produces("application/xml")
    public List<Students> getAll() {
        return students.findAll();
    }
    
    
    
    @GET
    @Path("{id}")
    @Produces("application/xml")
    public Students getById(@PathParam("id") Integer id) {
        return students.find(id);
    }
    
//    @GET
//    @Path("{user}")
//    @Produces("text/plain")
//    public String meno(@PathParam("user") String user){
//        return user;
//    }
    
//    @PUT
//    @Path("{user}")
//    @Consumes("application/xml")
//    public String create(@PathParam("user") String user,Students entity) {
//        if (users.find(user)  != null) {
//            students.create(entity);
//            return "user:"+user+" - access granted, new student saved to DB!";
//        } else {
//            return "user:"+user+" - access denied";
//        }
//    }
    
    @POST
    @Path("/{username : [A-Za-z0-9_]*}/students")
    @Consumes("application/xml;charset=UTF-8")
    public String create(@PathParam("username") String username,Students newStudent) {
        if (users.find(username) != null) {
            students.edit(newStudent);
            return "access granted - student: "+newStudent.getSurname()+" "+newStudent.getLastname()+ " added";
        } else {
            return "access denied";
        }
    }
    
    @PUT
    @Path("/{username : [A-Za-z0-9_]*}/students/{id}")
    @Consumes("application/xml;charset=UTF-8")
    public String edit(@PathParam("username") String username, @PathParam("id") Integer studentId, Students newStudent) {
        if (users.find(username) != null) {
            Students studentToEdit = students.find(studentId);
            
            studentToEdit.setAddress(newStudent.getAddress());
            studentToEdit.setMobile(newStudent.getMobile());
            studentToEdit.setSurname(newStudent.getSurname());
            studentToEdit.setLastname(newStudent.getLastname());
            
            students.edit(studentToEdit);
            
            return "access granted - student: "+studentToEdit.getSurname()+" "+studentToEdit.getLastname()+ " edited";
        } else {
            return "access denied";
        }
    }
    
    @DELETE
    @Path("/{username : [A-Za-z0-9_]*}/students/{id}")
    public String delete(@PathParam("username") String username, @PathParam("id") Integer studentId) {
        if (users.find(username) != null) {
            Students studentToDelete = students.find(studentId);
            students.remove(studentToDelete);
            
            return "access granted - student successfully deleted";
        } else {
            return "access denied";
        }
    }

    

    
}
