/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package asszadanie1_client;

import entities.Students;
import java.util.Scanner;
import rest_client.NewJerseyClient;




/**
 *
 * @author husticik
 */
public class ClientManager {
    
    private NewJerseyClient client;
    private String admin;
    
    public ClientManager(NewJerseyClient client) {
        this.client = client;
        this.admin = "michal";
    }
    
    public void getAllStudents() {
        Students[] students = this.client.getAll(Students[].class);
        System.out.println("\nSTUDENTS\n");
        System.out.println("ID, SURNAME LASTNAME, ADDRESS ,MOBILE\n");
        for (Students student : students) {
            String output = student.getId()+", "+student.getSurname()+" "+student.getLastname()+", "+student.getAddress()+", "+student.getMobile();
            System.out.println(output);
        }
        System.out.println("*************\n");
    }
    
    public void getStudentById() {
        Scanner scanner  = new Scanner(System.in);
        System.out.print("Which ID? : ");
        String Id = scanner.next();
        Students student = this.client.getById(Students.class, Id);
        
        if (student != null) {
            System.out.println("STUDENT");
            String output = student.getSurname()+" "+student.getLastname()+", "+student.getAddress()+", "+student.getMobile();
            System.out.println(output);
        } else {
            System.out.println("SORRY, STUDENT WITH ID "+Id+" DOES NOT EXIST");
            
        }
        System.out.println("**********\n");
        
    }
    
    public void createStudent() {
        Scanner scanner  = new Scanner(System.in);
        String input;
        Students student = new Students();
        System.out.println("NEW STUDENT");
        
        System.out.print("SURNAME: ");
        input = scanner.nextLine();
        student.setSurname(input);
        
        System.out.print("LASTNAME: ");
        input = scanner.nextLine();
        student.setLastname(input);
        
        System.out.print("ADDRESS: ");
        input = scanner.nextLine();
        student.setAddress(input);
        
        System.out.print("MOBILE: ");
        input = scanner.nextLine();
        student.setMobile(input);
        
        Integer newId = ((this.client.getAll(Students[].class)).length) + 1;
        
        student.setId(newId);
        
        this.client.create(student,admin);
        
        String output = student.getSurname()+" "+student.getLastname()+", "+student.getAddress()+", "+student.getMobile();
        
        System.out.println("STUDENT: "+ output+" CREATED");
        System.out.println("************\n1");
        
        
    }
    
    public void editStudentById() {
        Scanner scanner  = new Scanner(System.in);
        System.out.print("Which ID? : ");
        String Id = scanner.nextLine();
        Students student = this.client.getById(Students.class, Id);
        
        if (student != null) {
            
            System.out.println("CHOSEN STUDENT");
            String output = student.getSurname()+" "+student.getLastname()+", "+student.getAddress()+", "+student.getMobile();
            System.out.println(output);
            
            String chosen;
            String input;
            
            System.out.print("CHANGE SURNAME? (y/n): ");
            chosen = scanner.nextLine();
            
            if (chosen.equalsIgnoreCase("y")) {
                System.out.print("NEW SURNAME: ");
                input = scanner.nextLine();
                student.setSurname(input);
            }
            
            System.out.print("CHANGE LASTNAME? (y/n): ");
            chosen = scanner.nextLine();
            
            if (chosen.equalsIgnoreCase("y")) {
                System.out.print("NEW LASTNAME: ");
                input = scanner.nextLine();
                student.setLastname(input);
            }
            
            System.out.print("CHANGE ADDRESS? (y/n): ");
            chosen = scanner.nextLine();
            
            if (chosen.equalsIgnoreCase("y")) {
                System.out.print("NEW ADDRESS: ");
                input = scanner.nextLine();
                student.setAddress(input);
            }
            
            System.out.print("CHANGE MOBILE? (y/n): ");
            chosen = scanner.nextLine();
            
            if (chosen.equalsIgnoreCase("y")) {
                System.out.print("NEW MOBILE: ");
                input = scanner.nextLine();
                student.setMobile(input);
            }
            
            System.out.println("EDITED STUDENT IS");
            output = student.getSurname()+" "+student.getLastname()+", "+student.getAddress()+", "+student.getMobile();
            System.out.println(output);
            System.out.println("SAVE? (y/n)");
            
            chosen = scanner.nextLine();
            
            if (chosen.equalsIgnoreCase("y")) {
                this.client.edit(student,admin,Id);
            }
        } else {
            System.out.println("SORRY STUDENT WIT ID: "+Id+" DOES NOT EXIST");
        }
        System.out.println("*************\n");
    }
    
    public void deleteStudentById() {
        Scanner scanner  = new Scanner(System.in);
        System.out.print("Which ID? : ");
        String Id = scanner.nextLine();
        Students student = this.client.getById(Students.class, Id);
        
        if (student != null) {
            String output = student.getSurname()+" "+student.getLastname()+", "+student.getAddress()+", "+student.getMobile();
            System.out.println("DO YOU REALLY WANT TO DELETE");
            System.out.println(output);
            System.out.print("(y/n): ");
            
            String chosen = scanner.nextLine();
            
            if (chosen.equalsIgnoreCase("y")) {
                this.client.delete(admin, Id);
                System.out.println("STUDENT DELETED!");
            }
            
            
        } else {
            System.out.println("SORRY STUDENT WIT ID: "+Id+" DOES NOT EXIST");
        }
        
        System.out.println("************\n");
    }
    
}
