/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package asszadanie1_client;

import entities.Students;
import entities.Users;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import rest_client.NewJerseyClient;




/**
 *
 * @author husticik
 */
public class Asszadanie1_client {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("WELCOME TO CLIENT\n");
        
        NewJerseyClient client = new NewJerseyClient();
        ClientManager clientManager = new ClientManager(client);
        
        Scanner scanner = new Scanner(System.in);
        String menuItem;        
        do{
            showMenu();
            menuItem= scanner.nextLine();
            switch(menuItem){
                case "1": clientManager.getAllStudents();
                    break;
                case "2": clientManager.getStudentById();
                    break;
                case "3": clientManager.createStudent();
                    break;
                case "4": clientManager.editStudentById();
                    break;
                case "5": clientManager.deleteStudentById();
                    break;
                case "E":
                    break;
            }
        }while(!menuItem.equalsIgnoreCase("E"));
    }

    
    public static void showMenu(){
        System.out.println("1.Show all students");
        System.out.println("2.Show student by ID");
        System.out.println("3.Create student");
        System.out.println("4.Edit student by ID");
        System.out.println("5.Delete student by ID");
        System.out.println("E.Exit program\n");
    }
    
    
    
    
}


